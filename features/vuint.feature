Feature: Vuint - variable-length unsigned integer

Scenario Outline: Vuint round trip
    Given an unsigned integer '<n>'
    When I serialize it as vuint
    Then the binary representation should be '<b>'
    When I deserialize it as vuint
    Then the decimal representation should be '<n>'

    Examples:
    |      n |             b |
    |      1 |             1 |
    |      0 |             0 |
    |    127 |           127 |
    |    128 |        129, 0 |
    |    254 |      129, 126 |
    |    255 |      129, 127 |
    |    256 |        130, 0 |
    |    257 |        130, 1 |
    |    511 |      131, 127 |
    |    512 |        132, 0 |
    |  23363 |  129, 182, 67 |
    |  32766 | 129, 255, 126 |
    |  32767 | 129, 255, 127 |
    |  32768 |   130, 128, 0 |
    |  65535 | 131, 255, 127 |
    |  65536 |   132, 128, 0 |
