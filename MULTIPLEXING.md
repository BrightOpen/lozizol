# Multiplexing

This is an optional extension to the Lozizol protocol.

Use cases:
 * Zero or more sequences to share one common sequence. Each part identifies
   the respective subsequence it belongs to.
 * Out of order transfer of entries (for instance over UDP). Even though the 
   parts are received out of order, they can be written sequentially because
   the sequence offset is preserved. Or the original sequence can be 
   reconstructed immediately with random access writes.
 * Shirnk a sequence stream by eliminating the deleted records, padding, 
   indexes...

The multiplexed sequences can be reconstructed into individual sequences as
each part carries with it the original position in the sequence.

Theoretically, the sequences could be infinitely nested. The application
must take care of managing the state and apply any constraints required. 
It must decide how to handle parts without starts, parent sequence end 
without child end... 

## Entries

### Subsequence start

* URI: `urn:lozizol:start`
* Properties:
  * sequence ID (`uuid`)

#### Record

  * size (`vuint`) - length of type and sequence ID
  * type (`vuint`) - type id assigned to subsequence start
  * sequence ID (binary 16 bytes `uuid`)

### Subsequence part

* URI: `urn:lozizol:part`
* Properties:
  * sequence ID (`uuid`)
  * position - offset within the sequence
  * list of entries

#### Record

  * size (`vuint`) - length of type, sequence ID and all contained records
  * type (`vuint`) - type id assigned to subsequence part
  * sequence ID (binary 16 bytes `uuid`)
  * records of contained entries

### Subsequence end

URI: `urn:lozizol:end`
  * sequence ID (`uuid`)
  
#### Record

  * size (`vuint`) - length of type and sequence ID
  * type (`vuint`) - type id assigned to subsequence end
  * sequence ID (binary 16 bytes `uuid`)
