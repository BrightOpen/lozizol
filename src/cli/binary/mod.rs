#[derive(Debug)]
pub struct Binary(pub Vec<u8>);

impl std::str::FromStr for Binary {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.as_bytes() {
            [b'0', b'x', ..] | [b'0', b'X', ..] => {
                if let Ok(bytes) = hex::decode(&s[2..]) {
                    Ok(Binary(bytes))
                } else {
                    Err("Hexadecimal value is invalid".into())
                }
            }
            _ => Err("Prefix the value with format, such as 0x...".into()),
        }
    }
}
