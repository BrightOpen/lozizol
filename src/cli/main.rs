use std::process::exit;

use lozizol::cli::command::Cmd;
use structopt::StructOpt;

#[async_std::main]
pub async fn main() {
    env_logger::init();

    let cmd = Cmd::from_args();

    match cmd.run().await {
        Ok(()) => { /*happy*/ }
        Err(e) => {
            eprintln!("{}", e);
            exit(1)
        }
    }
}
