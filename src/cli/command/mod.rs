pub mod decode;
pub mod encode;
use structopt::StructOpt;

/// Loziziol CLI tool
#[derive(StructOpt, Debug)]
#[structopt(name = "lozizol")]
pub enum Cmd {
    Encode(encode::Cmd),
    Decode(decode::Cmd),
}

impl Cmd {
    pub async fn run(self) -> anyhow::Result<()> {
        match self {
            Cmd::Encode(cmd) => cmd.run().await,
            Cmd::Decode(cmd) => cmd.run().await,
        }
    }
}
