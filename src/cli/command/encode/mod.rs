use crate::task;
use async_std::io::{prelude::WriteExt, stdin, stdout, ReadExt};
use structopt::StructOpt;

/// Serialize into Lozizol format
#[derive(StructOpt, Debug)]
#[structopt(name = "encode")]
pub enum Cmd {
    /// Take a decimal value and turn it into a vuint.
    /// Vuints are used for sizes and types.
    Vuint {
        /// The unsigned integer number to represent as vuint.
        /// If not set, read from stdin. Examples: 1234567
        number: Option<usize>,
    },
    /// Serialize an entry into a record
    Entry {
        /// Decimal representation of the entry type.
        #[structopt(name = "type")]
        entry_type: usize,
        /// Entry payload data. If not provided, read from stdin
        data: Option<String>,
    },
    /// Serialize type assignment as a record
    Type {
        /// Decimal representation of the entry type.
        #[structopt(name = "type")]
        entry_type: usize,
        /// Decimal representation of the newly assigned entry type.
        #[structopt(name = "assigned_type")]
        assigned_type: usize,
        /// URI of the type assigned
        uri: Option<url::Url>,
    },
}

impl Cmd {
    pub async fn run(self) -> anyhow::Result<()> {
        let mut output = stdout();
        let mut input = stdin();
        match self {
            Cmd::Vuint { number } => {
                let number = match number {
                    Some(number) => number,
                    None => {
                        let mut uint = [0u8; std::mem::size_of::<usize>()];
                        input.read_exact(&mut uint[..]).await?;
                        usize::from_be_bytes(uint)
                    }
                };
                task::encode::vuint(&mut output, &number).await?
            }
            Cmd::Entry { entry_type, data } => {
                let data = match data {
                    Some(data) => data.to_owned().into(),
                    None => {
                        let mut data = vec![];
                        input.read_to_end(&mut data).await?;
                        data
                    }
                };
                task::encode::record(&mut output, &entry_type, &data.len())
                    .await?
                    .write_all(data.as_slice())
                    .await?
            }
            Cmd::Type {
                entry_type,
                assigned_type,
                uri,
            } => {
                let uri = match uri {
                    Some(uri) => uri,
                    None => {
                        let mut uri = String::new();
                        input.read_to_string(&mut uri).await?;
                        uri.parse()?
                    }
                };
                task::encode::type_assingment(
                    &mut output,
                    &entry_type,
                    &assigned_type,
                    uri.as_str(),
                )
                .await?;
            }
        }
        Ok(output.flush().await?)
    }
}
