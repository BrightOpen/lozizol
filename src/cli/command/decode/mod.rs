use crate::{
    cli::binary::Binary,
    cli::error::{ErrorContext, Message},
    model::Vuint,
    task,
};
use async_std::io::{self, prelude::WriteExt, stdin, stdout, ReadExt};
use structopt::StructOpt;

/// Deserialize from Lozizol format
#[derive(StructOpt, Debug)]
#[structopt(name = "decode")]
pub enum Cmd {
    /// Take a vuint value and turn it into an unsigned integer number.
    /// Vuints are used for sizes and types.
    Vuint {
        /// The unsigned integer number to represent as vuint.
        /// If not set, read from stdin. Examples: 0x8654
        vuint: Option<Vuint<'static>>,
    },
    /// Deserialize an entry type and data
    Entry { input: Option<Binary> },
}

impl Cmd {
    pub async fn run(self) -> anyhow::Result<()> {
        match self {
            Cmd::Vuint { vuint } => {
                let number: u128 = match vuint {
                    Some(vuint) => task::decode::number(&mut vuint.as_ref()).await,
                    None => task::decode::number(&mut stdin()).await,
                }
                .context("reading vuint")?;
                stdout()
                    .write_all(number.to_be_bytes().as_ref())
                    .await
                    .context("writing uint")?;
                Ok(())
            }
            Cmd::Entry { input } => {
                let input = match input {
                    Some(input) => input.0,
                    None => {
                        let mut input = vec![];
                        stdin()
                            .read_to_end(&mut input)
                            .await
                            .context("reading input")?;
                        input
                    }
                };
                let x = match task::decode::record(&mut input.as_slice()).await {
                    Some(Ok(record)) => {
                        stdout()
                            .write_all(format!("{} ", record.type_id()).as_bytes())
                            .await
                            .context("writing type")?;
                        io::copy(record, stdout()).await.context("writing data")?;

                        Ok(())
                    }
                    Some(Err(e)) => Err(e).context("reading record")?,
                    None => Err(Message("reading record - no input"))?,
                };
                x
            }
        }
    }
}
