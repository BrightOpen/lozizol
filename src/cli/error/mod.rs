use std::fmt;

/**
An error with a context

Example:
```rust
# use lozizol::cli::error::{CtxError,Message};
let err = CtxError::new("ctx",Message("err"));
assert_eq!(format!("{}", err), "Error in ctx: err");
```
*/
#[derive(Debug)]
pub struct CtxError<C, E> {
    context: C,
    error: E,
}

impl<C, E> fmt::Display for CtxError<C, E>
where
    C: fmt::Display,
    E: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error in {}: {}", self.context, self.error)
    }
}

impl<C, E> std::error::Error for CtxError<C, E>
where
    C: fmt::Display + fmt::Debug,
    E: std::error::Error + 'static,
{
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(&self.error)
    }
}

impl<C, E> CtxError<C, E>
where
    C: fmt::Display + fmt::Debug,
    E: std::error::Error + 'static,
{
    /**
    Create new error with a context

    Example:
    ```rust
    # use lozizol::cli::error::{CtxError,Message};
    let _ = CtxError::new("ctx",Message("err"));
    let _ = CtxError::new(987u16,Message(true));
    ```
    */
    pub fn new(context: C, error: E) -> Self {
        Self { context, error }
    }
}

/**
Add some context information to an error

Example:
```rust
use lozizol::cli::error::ErrorContext;
let err = std::io::Error::new(std::io::ErrorKind::Other, "xyz");
let result: Result<(), _> = Err(err).context("here");
assert_eq!(format!("{}", result.err().unwrap()), "Error in here: xyz");
```
*/
pub trait ErrorContext {
    type Value;
    type Error;
    fn context<C>(self, context: C) -> Result<Self::Value, CtxError<C, Self::Error>>
    where
        C: fmt::Display + fmt::Debug;
}

impl<T, E> ErrorContext for Result<T, E>
where
    E: std::error::Error + 'static,
{
    type Value = T;
    type Error = E;
    fn context<C>(self, context: C) -> Result<Self::Value, CtxError<C, Self::Error>>
    where
        C: fmt::Display + fmt::Debug,
    {
        match self {
            Ok(v) => Ok(v),
            Err(error) => Err(CtxError::new(context, error)),
        }
    }
}

impl<T> ErrorContext for Option<T> {
    type Value = T;
    type Error = Message<&'static str>;
    fn context<C>(self, context: C) -> Result<Self::Value, CtxError<C, Self::Error>>
    where
        C: fmt::Display + fmt::Debug,
    {
        match self {
            Some(v) => Ok(v),
            None => Err(CtxError::new(context, Message("value is not set"))),
        }
    }
}

#[derive(Debug)]
pub struct Message<T>(pub T);

impl<T> fmt::Display for Message<T>
where
    T: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<T> std::error::Error for Message<T> where T: fmt::Display + fmt::Debug {}
