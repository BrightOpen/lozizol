/*!

# Implementation of the lozizol protocol

See README.md for detailed description of the protocol.

The crate is mostly intended to be used as a library. Most notably the Vuint type.
It also provides a CLI lozizol to play with the implemented features.

# lozizol CLI

```bash
cargo install lozizol
lozizol help
lozizol help serialize
lozizol help serialize vuint
```

*/

#[macro_use]
extern crate log;

#[cfg(feature = "cli")]
pub mod cli;
pub mod model;
#[cfg(feature = "tasks")]
pub mod task;
