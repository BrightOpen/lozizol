use crate::model::{data::Data, Vuint};

pub struct Record<R, O> {
    pub(crate) data: Data<R, O>,
    length: usize,
    pub(crate) remaining: usize,
    type_id: usize,
}
impl<R, O> Record<R, O> {
    pub fn owned(length: usize, type_id: usize, data: O) -> Self {
        Record::new(length, type_id, Data::Owned(data))
    }
    pub fn new(length: usize, type_id: usize, mut data: Data<R, O>) -> Self {
        if length == 0 {
            data = Data::None
        }
        Record {
            length,
            remaining: length,
            type_id,
            data,
        }
    }
    pub fn take_data(&mut self) -> Data<R, O> {
        self.remaining = 0;
        std::mem::take(&mut self.data)
    }
    pub fn type_id(&self) -> &usize {
        &self.type_id
    }
    pub fn len(&self) -> &usize {
        &self.length
    }
    pub fn len_including_header(&self) -> usize {
        self.length
            + Vuint::bytes_needed(&self.length)
            + if self.length + self.type_id == 0 {
                0
            } else {
                Vuint::bytes_needed(&self.type_id)
            }
    }
    pub fn remaining(&self) -> &usize {
        &self.remaining
    }
}
impl<R, O> Drop for Record<R, O> {
    fn drop(&mut self) {
        if self.remaining != 0 {
            error!(
                "Record data {} of {} remain unprocessed.",
                self.remaining, self.length
            )
        }
    }
}
