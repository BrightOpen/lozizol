use crate::model::{assignments::TypeAssignments, header::Header};
use std::error::Error;

#[derive(Debug, Clone)]
pub struct Sequence {
    assignments: TypeAssignments,
    version: String,
    id: String,
    info: String,
    read: usize,
    write: usize,
}

impl Sequence {
    pub fn new() -> Self {
        Self {
            assignments: TypeAssignments::default(),
            id: "00000000-0000-0000-0000-000000000000".into(),
            version: "0.5".to_owned(),
            info: String::new(),
            read: 0,
            write: 0,
        }
    }
    pub fn reset_from_header(&mut self, header: &Header) -> &mut Self {
        self.assignments.initialize();
        self.id = header.id().to_owned();
        self.version = header.version().to_owned();
        self.info = header.info().to_owned();
        self
    }
    pub fn validate_version<T: AsRef<[u8]>>(version: T) -> Result<T, SequenceVersionError<T>> {
        let v = version.as_ref();
        if v.len() >= 1 && v.len() < 60 && v[0].is_ascii_digit() && v.is_ascii() {
            Ok(version)
        } else {
            Err(SequenceVersionError(version))
        }
    }
    pub fn validate_id<T: AsRef<[u8]>>(id: T) -> Result<T, SequenceIdError<T>> {
        let i = id.as_ref();
        /*
        00000000-0000-0000-0000-000000000000
        */
        if i.len() == 36
            && i[8] == b'-'
            && i[13] == b'-'
            && i[18] == b'-'
            && i[23] == b'-'
            && i[0..8].iter().all(u8::is_ascii_hexdigit)
            && i[9..13].iter().all(u8::is_ascii_hexdigit)
            && i[14..18].iter().all(u8::is_ascii_hexdigit)
            && i[19..23].iter().all(u8::is_ascii_hexdigit)
            && i[25..].iter().all(u8::is_ascii_hexdigit)
        {
            Ok(id)
        } else {
            Err(SequenceIdError(id))
        }
    }

    pub fn assignments_mut(&mut self) -> &mut TypeAssignments {
        &mut self.assignments
    }
    pub fn assignments(&mut self) -> &TypeAssignments {
        &self.assignments
    }
    pub fn version(&self) -> &str {
        self.version.as_str()
    }
    pub fn set_version(
        &mut self,
        version: String,
    ) -> Result<&mut Self, SequenceVersionError<String>> {
        self.version = Self::validate_version(version)?;
        Ok(self)
    }
    pub fn info(&self) -> &str {
        self.info.as_str()
    }
    pub fn set_info(&mut self, info: String) -> &mut Self {
        self.info = info;
        self
    }
    pub fn id(&self) -> &str {
        self.id.as_str()
    }
    pub fn set_id(&mut self, id: String) -> Result<&mut Self, SequenceIdError<String>> {
        self.id = Self::validate_id(id)?;
        Ok(self)
    }
    pub fn read(&self) -> usize {
        self.read
    }
    pub fn set_read(&mut self, read: usize) -> &mut Self {
        self.read = read;
        self
    }
    pub fn advance_read(&mut self, length: usize) -> &mut Self {
        self.read += length;
        self
    }
    pub fn write(&self) -> usize {
        self.write
    }
    pub fn set_write(&mut self, write: usize) -> &mut Self {
        self.write = write;
        self
    }
    pub fn advance_write(&mut self, length: usize) -> &mut Self {
        self.write += length;
        self
    }
}

#[derive(Debug)]
pub struct SequenceIdError<T>(pub T);
impl<T> Error for SequenceIdError<T> where T: std::fmt::Debug {}
impl<T> std::fmt::Display for SequenceIdError<T>
where
    T: std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Sequence ID is invalid: {:?}", self.0)
    }
}

#[derive(Debug)]
pub struct SequenceVersionError<T>(pub T);
impl<T> Error for SequenceVersionError<T> where T: std::fmt::Debug {}
impl<T> std::fmt::Display for SequenceVersionError<T>
where
    T: std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Sequence version is invalid: {:?}", self.0)
    }
}
