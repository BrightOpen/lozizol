use super::Vuint;
use crate::model::uint::UnsignedInt;
use bytes::BufMut;

impl<'a> Vuint<'a> {
    /// Write `val` to the `std::io::Write` stream `w` as an vuint-encoded
    /// integer.
    ///
    /// [1]: enum.WriteError.html#variant.Negative
    ///
    /// # Returns
    /// After a successful write, the number of bytes written to `w` is returned.
    ///
    /// # Examples
    ///
    /// Writing a u8 and an i128 into three bytes.
    ///
    /// ```
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use lozizol::model::Vuint;
    ///
    /// let mut buffer = [0u8; 3];
    /// let mut writeable = &mut buffer[..];
    ///
    /// let bytes_written = Vuint::encode(&mut writeable, &127u8);
    /// assert_eq!(bytes_written, 1);
    ///
    /// let bytes_written = Vuint::encode(&mut writeable, &256u128);
    /// assert_eq!(bytes_written, 2);
    ///
    /// let mut readable = &buffer[..];
    /// assert_eq!(127u8, Vuint::decode(&mut readable)?);
    /// assert_eq!(256u16, Vuint::decode(&mut readable)?);
    /// # Ok(())
    /// }
    /// ```
    pub fn encode<W, I>(mut writer: W, input: &I) -> usize
    where
        W: BufMut,
        I: UnsignedInt,
    {
        use std::ops::BitOr;
        // filter for 7 bits
        let mask = I::from(0x7F).expect("mask");
        // number of bytes needed to encode this value in 7 bit chunks
        let cnt = Vuint::bytes_needed(input);
        for i in (0..cnt).rev() {
            writer.put_u8(
                input
                    // move the selected part to bottom byte position
                    .shr(i * 7)
                    // trim to 7 bits
                    .bitand(mask)
                    // we need one byte value
                    .to_u8()
                    // must fit to one byte
                    .expect("u8 conversion")
                    // add marker bit except for the last byte
                    .bitor(if i == 0 { 0 } else { 0x80 }),
            );
        }
        cnt
    }
}

#[test]
fn test_simple() {
    let mut buf = vec![];
    let len = Vuint::encode(&mut buf, &127u128);
    assert_eq!(buf, [127]);
    assert_eq!(len, 1);
}

#[test]
fn test_writing() {
    let testcases = vec![
        (0, 1, [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (1, 1, [0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (127, 1, [0x7F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (128, 2, [0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (255, 2, [0x81, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (256, 2, [0x82, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (16383, 2, [0xFF, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (16384, 3, [0x81, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (16511, 3, [0x81, 0x80, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (65535, 3, [0x83, 0xFF, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x00]),
        (
            1u64 << 32,
            5,
            [0x90, 0x80, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00],
        ),
    ];

    for tc in testcases {
        let mut buf = [0u8; 8];
        let len = Vuint::encode(&mut buf[..], &tc.0);
        // check the contents of the written data
        assert_eq!(tc.2, buf);
        // check the length of the written data
        assert_eq!(tc.1, len);
    }
}

#[test]
fn test_write_and_then_read() {
    let mut buf = [0u8; 4096];

    let mut testcases = vec![];
    for i in 2..128 {
        testcases.push((1u128 << i) - 1);
        testcases.push(1u128 << i);
        testcases.push((1u128 << i) + 1);
    }

    // write testcases into buf
    let mut writable = &mut buf[..];
    for tc in testcases.clone() {
        Vuint::encode(&mut writable, &tc);
    }

    // read testcases from buf and check
    let mut readable = &buf[..];
    for tc in testcases {
        let val: u128 = Vuint::decode(&mut readable).unwrap();
        assert_eq!(tc, val);
    }
}
