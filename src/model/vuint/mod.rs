/*!

# Variable length encoding/decoding for unsigned integers

Positive, primitive Rust integers in the
Big Endian Base 128 variable-length encoding without
corrections for redundancy (not subtracting/adding one).

Each 7 bits are encoded into 7 bit bytes. The highest bit indicates if more bytes
follow. Reading stops after a byte with the highest bit unset is read. Errors on invalid
values, insufficient bytes and target integer type overflow.

Minimal dependencies with default features:
```bash
cargo tree -e no-dev -e no-build
```
   ├── bytes v1.0.1
   ├── hex v0.4.2
   ├── log v0.4.14
   │   └── cfg-if v1.0.0
   ├── memchr v2.3.4
   └── num-traits v0.2.14

Features:
* tasks - higher level operations mostly bound to async IO
* cli - command line interface

See also:
* [Variable Length Quantity](https://en.wikipedia.org/wiki/Variable-length_quantity)
* [Little Endian Base 128](https://en.wikipedia.org/wiki/LEB128)

# Company

And why it doesn't suit:

* [unsigned-varint](https://crates.io/crates/unsigned-varint)
   (part of [MultiFormats](https://github.com/multiformats/unsigned-varint)) -
   little endian, capped at u64
* [msb128](https://crates.io/crates/msb128) - +-one
* [spinifex-unsigned-varint](https://crates.io/crates/spinifex-unsigned-varint) - MultiFormats style
* [varinteger](https://crates.io/crates/varinteger) - u64 only
* [leb128](https://crates.io/crates/leb128) - u64 only, little endian
* [varuint](https://crates.io/crates/varuint) - marker bytes packed in the beginning

# Credits

Taken and adapted from https://crates.io/crates/msb128 by 0xB10C under MIT OR Apache-2.0 license.
Removed sub/add one. Specialized to unsigned ints to eliminate decoding of negative values.

*/
mod convert;
mod de;
mod en;
mod error;
mod format;
mod parse;
mod vuint;

pub use self::convert::*;
pub use self::de::*;
pub use self::en::*;
pub use self::error::*;
pub use self::format::*;
pub use self::parse::*;
pub use self::vuint::*;
