use super::Vuint;

/**
Enables binary formatting of Vuint with `"{:b}"`
* Supports alternate formating `"{:#b}"`
* Supports padding `"{:>100b}"`
Examples:
```rust
# use lozizol::model::Vuint;
assert_eq!(format!("{:b}", Vuint::from(0xFFFFu16)), "10000011_11111111_01111111");
assert_eq!(format!("{:#b}", Vuint::from(0xFFFFu16)), "[0b_10000011, 0b_11111111, 0b_01111111]");
assert_eq!(format!("{:>30b}", Vuint::from(0xFFFFu16)), "    10000011_11111111_01111111");
*/
impl<'a> std::fmt::Binary for Vuint<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use std::fmt::Write;
        let mut result = String::new();
        let mut first = true;
        if f.alternate() {
            write!(&mut result, "[")?;
        }
        for b in self.as_ref().iter() {
            if first {
                first = false;
            } else {
                if f.alternate() {
                    write!(&mut result, ", ")?;
                } else {
                    write!(&mut result, "_")?;
                }
            }
            if f.alternate() {
                write!(&mut result, "0b_")?;
            }
            write!(&mut result, "{:0>8b}", b)?;
        }
        if f.alternate() {
            write!(&mut result, "]")?;
        }
        f.pad(result.as_str())
    }
}

#[test]
fn test_binary_formatting() {
    use crate::model::uint::UnsignedInt;
    let vuints = [
        Vuint::from(&0u8),
        (&1u8).into(),
        (&128u8).into(),
        (&256u16).into(),
        (&0xB10Cu16).into(),
        (&0x8000u16).into(),
        (&0xFFFFu16).into(),
        (&0xFFFFFFu32).into(),
        (&u64::max_value()).into(),
        (&u128::max_value()).into(),
    ]
    .iter()
    .fold(String::new(), |result, vuint| {
        let number = vuint.to_uint::<u128>().expect("works");
        result
            + format!("{:>170}", number.format_uint_binary()).as_str()
            + format!(
                " = uint {} ({} bytes)\n",
                number,
                match number {
                    0 => 1,
                    _ => (7 + 128 - number.leading_zeros()) / 8,
                }
            )
            .as_str()
            + format!(
                "{:>170} vuint ({} bytes)\n",
                format!("{:b}", vuint),
                vuint.as_ref().len()
            )
            .as_str()
    });

    k9::snapshot!(vuints, "
                                                                                                                                                                  00000000 = uint 0 (1 bytes)
                                                                                                                                                                  00000000 vuint (1 bytes)
                                                                                                                                                                  00000001 = uint 1 (1 bytes)
                                                                                                                                                                  00000001 vuint (1 bytes)
                                                                                                                                                                  10000000 = uint 128 (1 bytes)
                                                                                                                                                         10000001_00000000 vuint (2 bytes)
                                                                                                                                                         00000001_00000000 = uint 256 (2 bytes)
                                                                                                                                                         10000010_00000000 vuint (2 bytes)
                                                                                                                                                         10110001_00001100 = uint 45324 (2 bytes)
                                                                                                                                                10000010_11100010_00001100 vuint (3 bytes)
                                                                                                                                                         10000000_00000000 = uint 32768 (2 bytes)
                                                                                                                                                10000010_10000000_00000000 vuint (3 bytes)
                                                                                                                                                         11111111_11111111 = uint 65535 (2 bytes)
                                                                                                                                                10000011_11111111_01111111 vuint (3 bytes)
                                                                                                                                                11111111_11111111_11111111 = uint 16777215 (3 bytes)
                                                                                                                                       10000111_11111111_11111111_01111111 vuint (4 bytes)
                                                                                                   11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111 = uint 18446744073709551615 (8 bytes)
                                                                                 10000001_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_01111111 vuint (10 bytes)
                           11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111 = uint 340282366920938463463374607431768211455 (16 bytes)
10000011_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111_01111111 vuint (19 bytes)

");
}
