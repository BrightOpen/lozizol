use std::fmt;

/// Errors when converting or validating Vuint as Uint
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum VuintError {
    /// The number doesn't fir into the target type
    Overflow,
    /// More bytes are expected. In other words, the las byte has the highest bit set.
    Incomplete,
    /// Leading zero byte (0x80)
    Invalid,
}

impl fmt::Display for VuintError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            VuintError::Incomplete => write!(f, "insufficient bytes"),
            VuintError::Overflow => write!(f, "encoded value overflows the integer type"),
            VuintError::Invalid => write!(f, "leading zero (0x80)"),
        }
    }
}

impl std::error::Error for VuintError {}

impl From<VuintError> for std::io::Error {
    fn from(e: VuintError) -> Self {
        std::io::Error::new(std::io::ErrorKind::InvalidData, format!("{}", e))
    }
}
