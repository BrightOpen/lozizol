use super::{Vuint, VuintError};
use crate::model::uint::UnsignedInt;
pub use bytes::Buf;

impl<'a> Vuint<'a> {
    /// Read a variable length encoded unsigned integer from `r`.
    ///
    /// After a successful read, the read integer is returned.
    ///
    /// # Errors
    ///
    /// The interger primitive used in the function and returned by the function is
    /// defined by the caller. If the integer primitive overflows while reading the
    /// variable length integer, a [`ReadError::Overflow`][1] is returned.
    ///
    /// [1]: enum.ReadError.html#variant.Overflow
    ///
    /// # Examples
    ///
    /// ```
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use lozizol::model::Vuint;
    ///
    /// // 10, 20, 30
    /// let data = [0x0A, 0x14, 0x1E];
    /// let mut readable = &data[..];
    ///
    /// assert_eq!(10u16, Vuint::decode(&mut readable)?);
    /// assert_eq!(20u8, Vuint::decode(&mut readable)?);
    /// assert_eq!(30u32, Vuint::decode(&mut readable)?);
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// The reader can either be passed (1) as value or (2) as mutable reference.
    /// See [C-RW-VALUE](https://rust-lang.github.io/api-guidelines/interoperability.html#c-rw-value).
    /// With case (1), the function returns the first variable length integer from
    /// the data on each call. With the mutable reader reference from case (2),
    /// successive calls return the next value each time. Case (2) is the standard
    /// reader use-case.
    ///
    /// ```rust
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use lozizol::model::Vuint;
    ///
    /// let data = [
    ///     0x0D,       // 13
    ///     0x7F,       // 127
    ///     0x81, 0x00, // 128
    ///     0xFE, 0x7F  // 16255
    /// ];
    /// let mut readable = &data[..];
    ///
    /// // case (1): pass by value
    /// assert_eq!(0x0Du8, Vuint::decode(readable)?);
    /// assert_eq!(0x0Du8, Vuint::decode(readable)?);
    ///
    /// // case (2): pass by mutable reference
    /// assert_eq!(0x0Du64, Vuint::decode(&mut readable)?);
    /// assert_eq!(127u8, Vuint::decode(&mut readable)?);
    /// assert_eq!(128u32, Vuint::decode(&mut readable)?);
    /// assert_eq!(16255u16, Vuint::decode(&mut readable)?);
    /// # Ok(())
    /// # }
    ///
    /// ```
    pub fn decode<R, I>(mut reader: R) -> Result<I, VuintError>
    where
        R: Buf,
        I: UnsignedInt,
    {
        let mut number: I = I::zero();
        loop {
            if !reader.has_remaining() {
                break Err(VuintError::Incomplete);
            }

            // read the next byte from r into the buffer
            let buffer_value = reader.get_u8();

            if buffer_value == 0x80 && number == I::zero() {
                // The first byte decodes as zero, this is invalid.
                break Err(VuintError::Invalid);
            }
            // append the last 127 bits of the buffer to the number
            // (if it wouldn't overflow while doing so)
            if number.leading_zeros() < 7 {
                break Err(VuintError::Overflow);
            }
            number = (number << 7) | I::from(buffer_value & 0x7F).expect("u8 will always fit");

            // If the most signigicant bit is set, then another byte follows,
            // otherwise we are done
            if buffer_value & 0x80 == 0 {
                break Ok(number);
            }
        }
    }
}

#[test]
fn test_reading() {
    assert_eq!(128u128, Vuint::decode(&mut &[0x81, 0x00][..]).unwrap());
    assert_eq!(0u8, Vuint::decode(&mut &[0x00][..]).unwrap());
    assert_eq!(1u32, Vuint::decode(&mut &[0x01][..]).unwrap());
    assert_eq!(127u32, Vuint::decode(&mut &[0x7F][..]).unwrap());
    assert_eq!(128u32, Vuint::decode(&mut &[0x81, 0x00][..]).unwrap());
    assert_eq!(16255u32, Vuint::decode(&mut &[0xFE, 0x7F][..]).unwrap());
    assert_eq!(16256u32, Vuint::decode(&mut &[0xFF, 0x00][..]).unwrap());
    assert_eq!(16383u32, Vuint::decode(&mut &[0xFF, 0x7F][..]).unwrap());
    assert_eq!(
        49023u32,
        Vuint::decode(&mut &[0x82, 0xFE, 0x7F][..]).unwrap()
    );
    assert_eq!(
        4024418176u64,
        Vuint::decode(&mut &[0x8E, 0xFE, 0xFE, 0xFF, 0x00][..]).unwrap()
    );
}

#[test]
fn fail_test_zero_leading_byte() {
    assert!(Vuint::decode::<&[u8], u16>(&mut &[0x80, 0x01][..]).is_err());
    assert!(Vuint::decode::<&[u8], u16>(&mut &[0x80, 0x81, 0x7F][..]).is_err());
}

#[test]
fn fail_test_incomplete() {
    assert!(Vuint::decode::<&[u8], u16>(&mut &[0x81][..]).is_err());
    assert!(Vuint::decode::<&[u8], u16>(&mut &[0x81, 0x81, 0x81][..]).is_err());
}
