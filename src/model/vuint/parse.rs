use super::{Vuint, VuintError};
use fmt::Debug;
use std::{fmt, str};

impl str::FromStr for Vuint<'static> {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.as_bytes() {
            [b'0', b'x', ..] | [b'0', b'X', ..] => {
                if let Ok(bytes) = hex::decode(&s[2..]) {
                    Ok(Vuint::try_from_bytes(bytes)?)
                } else {
                    Err(ParseError::InvalidHex)
                }
            }
            _ => Err(ParseError::UnrecognizedInput),
        }
    }
}

/// Error when parsing vuint from string
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ParseError {
    InvalidHex,
    UnrecognizedInput,
    ConversionError(VuintError),
}

impl From<VuintError> for ParseError {
    fn from(e: VuintError) -> Self {
        ParseError::ConversionError(e)
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ParseError::ConversionError(e) => write!(f, " {}", e),
            ParseError::InvalidHex => write!(f, "Vuint parse error: hexadecimal value is invalid"),
            ParseError::UnrecognizedInput => {
                write!(f, "Vuint parse error: unrecognized input, try prefix the value with format, such as 0x...")
            }
        }
    }
}

impl std::error::Error for ParseError {}

#[test]
fn test_parse_vuint() {
    k9::assert_equal!("0x817F".parse::<Vuint>().unwrap(), Vuint::from(&255u8));
}

#[test]
fn fail_test_parse_vuint_unrecognized() {
    k9::assert_equal!("817F".parse::<Vuint>(), Err(ParseError::UnrecognizedInput));
}

#[test]
fn fail_test_parse_vuint_invalid_hex() {
    k9::assert_equal!("0x817_".parse::<Vuint>(), Err(ParseError::InvalidHex));
    k9::assert_equal!("0x817".parse::<Vuint>(), Err(ParseError::InvalidHex));
    k9::assert_equal!("0x817z".parse::<Vuint>(), Err(ParseError::InvalidHex));
}
