use super::VuintError;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Vuint<'a> {
    inner: VuintInner<'a>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum VuintInner<'a> {
    Owned(Vec<u8>),
    Borrowed(&'a [u8]),
}

impl Default for Vuint<'static> {
    fn default() -> Self {
        Self::from_bytes_unchecked(vec![0])
    }
}

impl Vuint<'static> {
    /// Internal function to create a Vuint from Vec
    /// assuming that the caller is certain about it's validity
    pub(crate) fn from_bytes_unchecked(bytes: Vec<u8>) -> Vuint<'static> {
        Self {
            inner: VuintInner::Owned(bytes),
        }
    }
}

impl<'a> Vuint<'a> {
    /// Internal function to create a Vuint from slice
    /// assuming that the caller is certain about it's validity
    pub(crate) fn from_slice_unchecked(bytes: &'a [u8]) -> Vuint<'a> {
        Self {
            inner: VuintInner::Borrowed(bytes),
        }
    }
    pub(crate) fn as_slice(&self) -> &[u8] {
        match self.inner {
            VuintInner::Owned(ref b) => b.as_slice(),
            VuintInner::Borrowed(b) => b,
        }
    }
    pub fn is_zero(&self) -> bool {
        match self.inner {
            VuintInner::Borrowed([0]) => true,
            VuintInner::Owned(ref b) if b.as_slice() == b"\0" => true,
            _ => false,
        }
    }
    pub fn validate(bytes: &[u8]) -> Result<(), VuintError> {
        match bytes {
            [] => {
                // need at least one byte
                Err(VuintError::Incomplete)
            }
            [0x80, ..] => {
                // leading zero byte
                Err(VuintError::Invalid)
            }
            [first, ..] if bytes.len() == 19 && first & 0b_0111_1100 != 0 => {
                // 19 bytes, but too large even for u128
                Err(VuintError::Overflow)
            }
            toolong if toolong.len() > 19 => {
                // too many bytes
                Err(VuintError::Overflow)
            }
            [.., last] | [last] if last & 0x80 != 0 => {
                // last byte does not terminate
                Err(VuintError::Incomplete)
            }
            [head @ .., _last] if head.iter().any(|b| b & 0x80 == 0x00) => {
                // not all head bytes are marked
                Err(VuintError::Invalid)
            }
            _ => Ok(()),
        }
    }
}
