use super::{Vuint, VuintError};
use crate::model::uint::UnsignedInt;

impl<'a> AsRef<[u8]> for Vuint<'a> {
    /// exposes the internal vuint byte slice
    fn as_ref(&self) -> &[u8] {
        self.as_slice()
    }
}

impl<'a, T> From<&'a T> for Vuint<'static>
where
    T: UnsignedInt,
{
    fn from(value: &T) -> Self {
        let mut buf = vec![];
        Vuint::encode(&mut buf, value);
        Self::from_bytes_unchecked(buf)
    }
}
impl From<usize> for Vuint<'static> {
    fn from(value: usize) -> Self {
        Self::from(&value)
    }
}
impl From<u128> for Vuint<'static> {
    fn from(value: u128) -> Self {
        Self::from(&value)
    }
}
impl From<u64> for Vuint<'static> {
    fn from(value: u64) -> Self {
        Self::from(&value)
    }
}
impl From<u32> for Vuint<'static> {
    fn from(value: u32) -> Self {
        Self::from(&value)
    }
}
impl From<u16> for Vuint<'static> {
    fn from(value: u16) -> Self {
        Self::from(&value)
    }
}
impl From<u8> for Vuint<'static> {
    fn from(value: u8) -> Self {
        Self::from(&value)
    }
}
impl From<bool> for Vuint<'static> {
    fn from(value: bool) -> Self {
        if value {
            Self::from(&1u8)
        } else {
            Self::default()
        }
    }
}

impl<'a> Vuint<'a> {
    /// Create vuint from vuint bytes Vec.
    /// It validates the bytes to conform to standard
    pub fn try_from_slice(value: &'a [u8]) -> Result<Self, VuintError> {
        Self::validate(value)?;
        Ok(Self::from_slice_unchecked(value))
    }
    pub fn to_uint<I: UnsignedInt>(&self) -> Result<I, VuintError> {
        Vuint::decode(self.as_ref())
    }
    pub fn bytes_needed<I: UnsignedInt>(number: &I) -> usize {
        if number.is_zero() {
            // zero is a special case, we need at least one byte even if zero
            return 1;
        }
        let size = std::mem::size_of::<I>();
        // number of bytes needed to encode this value in 7 bit chunks
        (8 * size - number.leading_zeros() as usize + 6) / 7
    }
}

impl Vuint<'static> {
    /// Create vuint from vuint bytes Vec.
    /// It validates the bytes to conform to standard
    pub fn try_from_bytes(value: Vec<u8>) -> Result<Self, VuintError> {
        Self::validate(value.as_slice())?;
        Ok(Self::from_bytes_unchecked(value))
    }
}
