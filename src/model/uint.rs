use num_traits::{PrimInt, Unsigned};

pub trait UnsignedInt: PrimInt + Unsigned {
    fn format_uint_binary(&self) -> String {
        let dump = self
            .to_u128()
            .expect("must work")
            .to_be_bytes()
            .iter()
            .fold(String::new(), |set, chunk| {
                if set.is_empty() && *chunk == 0 {
                    set
                } else if set.is_empty() {
                    format!("{:0>8b}", chunk)
                } else {
                    set + "_" + format!("{:0>8b}", chunk).as_str()
                }
            });
        if dump.is_empty() {
            "00000000".to_owned()
        } else {
            dump
        }
    }
}

impl<I> UnsignedInt for I where I: PrimInt + Unsigned {}
