use crate::model::sequence::Sequence;
use std::ops::Range;

#[derive(Debug, Clone)]
pub struct Header<'a> {
    data: HeaderData<'a>,
    version: Range<usize>,
    id: Range<usize>,
    info: Range<usize>,
}
#[derive(Debug, Clone)]
pub enum HeaderData<'a> {
    Borrowed(&'a [u8]),
    Owned(Vec<u8>),
}
impl Header<'static> {
    pub fn try_from_bytes(data: Vec<u8>) -> Result<Self, HeaderParseError> {
        let (version, id, info) = Self::init_from_slice(data.as_slice())?;

        Ok(Header {
            data: HeaderData::Owned(data),
            version,
            id,
            info,
        })
    }
}
impl<'a> Header<'a> {
    pub fn id(&self) -> &str {
        std::str::from_utf8(&self.as_bytes()[self.id.clone()]).expect("valid id")
    }
    pub fn version(&self) -> &str {
        std::str::from_utf8(&self.as_bytes()[self.version.clone()]).expect("valid version")
    }
    pub fn info(&self) -> &str {
        std::str::from_utf8(&self.as_bytes()[self.info.clone()]).expect("valid info")
    }
    pub fn try_from_slice(data: &'a [u8]) -> Result<Self, HeaderParseError> {
        let (version, id, info) = Self::init_from_slice(data)?;

        Ok(Header {
            data: HeaderData::Borrowed(data),
            version,
            id,
            info,
        })
    }
    pub fn as_bytes(&self) -> &[u8] {
        match self.data {
            HeaderData::Borrowed(ref data) => data,
            HeaderData::Owned(ref data) => data.as_slice(),
        }
    }
    pub fn into_vec(self) -> Vec<u8> {
        match self.data {
            HeaderData::Borrowed(data) => data.to_vec(),
            HeaderData::Owned(data) => data,
        }
    }
    fn init_from_slice(
        data: &[u8],
    ) -> Result<(Range<usize>, Range<usize>, Range<usize>), HeaderParseError> {
        let mut iter = memchr::memchr_iter(b' ', data);
        let s1 = iter.next().ok_or(HeaderParseError::MissingPart)?;
        let s2 = iter.next().ok_or(HeaderParseError::MissingPart)?;
        let s3 = iter.next().ok_or(HeaderParseError::MissingPart)?;

        let version = s1 + 1..s2;
        let id = s2 + 1..s3;
        let info = s3 + 1..data.len();
        Sequence::validate_version(&data[version.clone()]).map_err(|e| {
            HeaderParseError::InvalidVersion(String::from_utf8_lossy(e.0).to_string())
        })?;
        Sequence::validate_id(&data[id.clone()])
            .map_err(|e| HeaderParseError::InvalidId(String::from_utf8_lossy(e.0).to_string()))?;
        if data[info.clone()].is_ascii() {
            Ok((version, id, info))
        } else {
            Err(HeaderParseError::InvalidInfo)
        }
    }
}

#[derive(Debug)]
pub enum HeaderParseError {
    MissingPart,
    InvalidInfo,
    InvalidId(String),
    InvalidVersion(String),
}
