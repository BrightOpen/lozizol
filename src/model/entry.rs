use crate::model::{data::Data, record::Record};

pub struct Entry<R, O> {
    pub(crate) data: Data<R, O>,
    length: usize,
    pub(crate) remaining: usize,
    type_uri: String,
    position: usize,
}
impl<R, O> Entry<R, O> {
    pub fn owned(position: usize, length: usize, type_uri: String, data: O) -> Self {
        Entry::new(position, length, type_uri, Data::Owned(data))
    }
    pub fn new(position: usize, length: usize, type_uri: String, mut data: Data<R, O>) -> Self {
        if length == 0 {
            data = Data::None
        }
        Entry {
            position,
            length,
            remaining: length,
            type_uri,
            data,
        }
    }
    pub fn from_record(mut record: Record<R, O>, position: usize, type_uri: String) -> Self {
        let remainig = *record.remaining();
        let mut me = Entry::new(position, *record.len(), type_uri, record.take_data());
        me.remaining = remainig;
        me
    }
    pub fn take_data(&mut self) -> Data<R, O> {
        self.remaining = 0;
        std::mem::take(&mut self.data)
    }
    pub fn type_uri(&self) -> &str {
        self.type_uri.as_str()
    }
    pub fn len(&self) -> &usize {
        &self.length
    }
    pub fn position(&self) -> &usize {
        &self.position
    }
    pub fn remaining(&self) -> &usize {
        &self.remaining
    }
}
impl<R, O> Drop for Entry<R, O> {
    fn drop(&mut self) {
        if self.remaining != 0 {
            error!(
                "Entry data {} of {} remain unprocessed.",
                self.remaining, self.length
            )
        }
    }
}
