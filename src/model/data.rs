#[derive(Debug, Clone)]
pub enum Data<IO, T> {
    Owned(T),
    IO(IO),
    None,
}
impl<IO, T> Default for Data<IO, T> {
    fn default() -> Self {
        Data::None
    }
}
