use crate::model::{UnsignedInt, Vuint};
use async_std::io::{self, prelude::WriteExt};

pub async fn type_assingment<W, N, T>(
    mut output: W,
    entry_type: &N,
    assigned_type: &T,
    uri: &str,
) -> io::Result<usize>
where
    W: io::Write + Unpin,
    N: UnsignedInt,
    T: UnsignedInt,
{
    let entry_type = Vuint::from(entry_type);
    let assigned_type = Vuint::from(assigned_type);
    let len = entry_type.as_ref().len() + assigned_type.as_ref().len() + uri.len();
    let vlen = Vuint::from(&len);
    output.write_all(vlen.as_ref()).await?;
    output.write_all(entry_type.as_ref()).await?;
    output.write_all(assigned_type.as_ref()).await?;
    output.write_all(uri.as_bytes()).await?;
    Ok(len + vlen.as_ref().len())
}
