use crate::model::{Data, Record, Vuint};
use async_std::io::{self, prelude::WriteExt, Cursor};
use std::{
    pin::Pin,
    task::{Context, Poll},
};

pub async fn record<'a, 'w, W>(
    mut output: W,
    type_id: &usize,
    data_length: &usize,
) -> io::Result<Record<W, Cursor<Vec<u8>>>>
where
    W: io::Write + Unpin + 'w,
    'w: 'a,
{
    let vuint_type_id = Vuint::from(type_id);
    let vuint_length = Vuint::from(vuint_type_id.as_ref().len() + data_length);
    output.write_all(vuint_length.as_ref()).await?;
    output.write_all(vuint_type_id.as_ref()).await?;

    Ok(Record::new(*data_length, *type_id, Data::IO(output)))
}

impl<W> Record<W, Cursor<Vec<u8>>>
where
    W: io::Write + Unpin,
{
    pub async fn skip_write(&mut self) -> io::Result<()> {
        let mut buf = b"\0".repeat(self.remaining);
        self.write_all(&mut buf).await?;
        Ok(())
    }
}

impl<W> io::Write for Record<W, Cursor<Vec<u8>>>
where
    W: io::Write + Unpin,
{
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<io::Result<usize>> {
        if self.remaining == 0 {
            return Poll::Ready(Err(io::Error::new(
                io::ErrorKind::WriteZero,
                "Recod length has already been written",
            )));
        }
        let buf = &buf[..usize::min(buf.len(), self.remaining)];
        if let Poll::Ready(written) = Pin::new(&mut self.data).poll_write(cx, buf)? {
            self.remaining -= written;
            Poll::Ready(Ok(written))
        } else {
            Poll::Pending
        }
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        Pin::new(&mut self.data).poll_flush(cx)
    }

    fn poll_close(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        Pin::new(&mut self.data).poll_close(cx)
    }
}
