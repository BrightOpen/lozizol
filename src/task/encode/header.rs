use crate::model::{UnsignedInt, Vuint};
use async_std::io::{self, prelude::WriteExt, Write};

pub async fn header<W, N>(
    mut output: W,
    entry_type: &N,
    version: impl AsRef<str>,
    id: impl AsRef<str>,
    info: impl AsRef<[u8]>,
) -> io::Result<usize>
where
    W: Write + Unpin,
    N: UnsignedInt,
{
    let entry_type = Vuint::from(entry_type);
    let entry_type = entry_type.as_ref();
    let version = version.as_ref().as_bytes();
    let id = id.as_ref().as_bytes();
    let info = info.as_ref();

    let len = 8 + entry_type.len() + version.len() + id.len();
    if len > b'l' as usize {
        return Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "header type, version and id are too long",
        ));
    }

    let remaining = b'l' as usize - len;
    let data = &info[..usize::min(remaining, info.len())];

    output.write_all(b"l").await?;
    output.write_all(entry_type.as_ref()).await?;
    output.write_all(b"zizol ").await?;
    output.write_all(version).await?;
    output.write_all(b" ").await?;
    output.write_all(id).await?;
    output.write_all(b" ").await?;
    output.write_all(data).await?;
    output
        .write_all(b" ".repeat(remaining - data.len()).as_ref())
        .await?;
    Ok(b'l' as usize + 1)
}
