use crate::model::{Entry, Sequence};
use async_std::{
    io::{self, prelude::WriteExt, Cursor},
    stream::{Stream, StreamExt},
};
pub async fn sequence<'s, 'e, S, W, R>(
    sequence: &mut Sequence,
    mut writer: W,
    stream: &mut S,
) -> io::Result<()>
where
    W: io::Write + Unpin,
    R: io::Read + Unpin,
    S: Stream<Item = Entry<R, Cursor<Vec<u8>>>> + 's + Unpin,
    's: 'e,
{
    while let Some(entry) = stream.next().await {
        let mut w =
            crate::task::encode::entry(sequence, &mut writer, entry.type_uri(), &entry.len())
                .await?;
        io::copy(entry, &mut w).await?;
        w.flush().await?;
    }
    Ok(())
}
