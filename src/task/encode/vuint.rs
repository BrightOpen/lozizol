use crate::model::{UnsignedInt, Vuint};
use async_std::io::{self, prelude::WriteExt, Write};

pub async fn vuint<W, N>(mut output: W, number: &N) -> io::Result<()>
where
    W: Write + Unpin,
    N: UnsignedInt,
{
    Ok(output.write_all(Vuint::from(number).as_ref()).await?)
}
