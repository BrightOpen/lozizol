use crate::model::{Entry, Sequence};
use async_std::io::{self, prelude::WriteExt, Cursor};
use std::{
    pin::Pin,
    task::{Context, Poll},
};

pub async fn entry<'a, 'w, W>(
    sequence: &mut Sequence,
    mut writer: W,
    type_uri: impl AsRef<str>,
    data_length: &usize,
) -> io::Result<Entry<W, Cursor<Vec<u8>>>>
where
    W: io::Write + Unpin + 'w,
    'w: 'a,
{
    trace!("reading messages");

    if !sequence.assignments().is_initialized() {
        let len = crate::task::encode::header(
            &mut writer,
            &111usize,
            sequence.version(),
            sequence.id(),
            sequence.info(),
        )
        .await?;
        writer.flush().await?;
        sequence.advance_write(len);
        sequence.assignments_mut().initialize();
    }

    let assigned_id = match sequence
        .assignments()
        .get_id(type_uri.as_ref())
        .map(usize::to_owned)
    {
        Some(id) => id,
        None => {
            let assignment = sequence.assignments_mut().reserve();

            let len = crate::task::encode::type_assingment(
                &mut writer,
                assignment.type_assignment_type_id(),
                assignment.assigned_type_id(),
                type_uri.as_ref(),
            )
            .await?;
            writer.flush().await?;

            let id = *assignment.store(type_uri.as_ref().to_owned());
            sequence.advance_write(len);
            id
        }
    };

    let position = sequence.write();
    let record = crate::task::encode::record(writer, &assigned_id, data_length).await?;
    sequence.advance_write(record.len_including_header());
    Ok(Entry::from_record(
        record,
        position,
        type_uri.as_ref().to_owned(),
    ))
}

impl<W> io::Write for Entry<W, Cursor<Vec<u8>>>
where
    W: io::Write + Unpin,
{
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<io::Result<usize>> {
        if self.remaining == 0 {
            return Poll::Ready(Err(io::Error::new(
                io::ErrorKind::WriteZero,
                "Entry length has already been written",
            )));
        }
        let buf = &buf[..usize::min(buf.len(), self.remaining)];
        if let Poll::Ready(written) = Pin::new(&mut self.data).poll_write(cx, buf)? {
            self.remaining -= written;
            Poll::Ready(Ok(written))
        } else {
            Poll::Pending
        }
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        Pin::new(&mut self.data).poll_flush(cx)
    }

    fn poll_close(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        Pin::new(&mut self.data).poll_close(cx)
    }
}
