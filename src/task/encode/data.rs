use crate::model::Data;
use async_std::io::{self, Cursor};
use std::{
    pin::Pin,
    task::{Context, Poll},
};

impl<W> io::Write for Data<W, Cursor<Vec<u8>>>
where
    W: io::Write + Unpin,
{
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<io::Result<usize>> {
        match self.get_mut() {
            Data::Owned(ref mut c) => Pin::new(c).poll_write(cx, buf),
            Data::IO(ref mut r) => Pin::new(r).poll_write(cx, buf),
            Data::None => Poll::Ready(Err(io::ErrorKind::WriteZero.into())),
        }
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        match self.get_mut() {
            Data::Owned(ref mut c) => Pin::new(c).poll_flush(cx),
            Data::IO(ref mut r) => Pin::new(r).poll_flush(cx),
            Data::None => Poll::Ready(Ok(())),
        }
    }

    fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        match self.get_mut() {
            Data::Owned(ref mut c) => Pin::new(c).poll_close(cx),
            Data::IO(ref mut r) => Pin::new(r).poll_close(cx),
            Data::None => Poll::Ready(Ok(())),
        }
    }
}
