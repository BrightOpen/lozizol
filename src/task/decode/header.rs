use crate::model::{Header, HeaderParseError};
use async_std::io::{Read, ReadExt};

pub async fn header_payload<R>(
    mut input: R,
    len: usize,
) -> Result<Header<'static>, HeaderReadError>
where
    R: Read + Unpin,
{
    let mut data = vec![0u8; len];
    input.read_exact(&mut data).await?;
    Ok(Header::try_from_bytes(data)?)
}

use std::error::Error;

#[derive(Debug)]
pub enum HeaderReadError {
    Parse(HeaderParseError),
    Io(std::io::Error),
}

impl Error for HeaderReadError {}
impl std::fmt::Display for HeaderReadError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Sequence header read error: ")?;
        match self {
            HeaderReadError::Io(e) => write!(f, "IO: {}", e),
            HeaderReadError::Parse(e) => write!(f, "parsing: {:?}", e),
        }
    }
}

impl From<HeaderParseError> for HeaderReadError {
    fn from(e: HeaderParseError) -> Self {
        HeaderReadError::Parse(e)
    }
}
impl From<std::io::Error> for HeaderReadError {
    fn from(e: std::io::Error) -> Self {
        HeaderReadError::Io(e)
    }
}
