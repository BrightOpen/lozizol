use crate::{
    model::{UnsignedInt, Vuint},
    task::decode::DecodeError,
};
use async_std::io::Read;

pub async fn number<R, N>(input: R) -> Result<N, DecodeError>
where
    R: Read + Unpin,
    N: UnsignedInt,
{
    Ok(vuint(input).await?.to_uint()?)
}
pub async fn number_sync<R, N>(input: R) -> Result<N, DecodeError>
where
    R: std::io::Read + Unpin,
    N: UnsignedInt,
{
    Ok(vuint_sync(input).await?.to_uint()?)
}
pub async fn vuint<R>(mut input: R) -> Result<Vuint<'static>, DecodeError>
where
    R: Read + Unpin,
{
    use async_std::io::ReadExt;
    let mut vuint = vec![];
    let mut buf = [0u8];
    loop {
        input.read_exact(&mut buf).await?;
        vuint.push(buf[0]);
        if buf[0] & 0x80 == 0 {
            // last byte
            break Ok(Vuint::try_from_bytes(vuint)?);
        }
    }
}

pub async fn vuint_sync<R>(mut input: R) -> Result<Vuint<'static>, DecodeError>
where
    R: std::io::Read,
{
    let mut vuint = vec![];
    let mut buf = [0u8];
    loop {
        input.read_exact(&mut buf)?;
        vuint.push(buf[0]);
        if buf[0] & 0x80 == 0 {
            // last byte
            break Ok(Vuint::try_from_bytes(vuint)?);
        }
    }
}
