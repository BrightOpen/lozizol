use crate::model::Data;
use async_std::io::{self, Cursor};
use std::{
    pin::Pin,
    task::{Context, Poll},
};

impl<R> io::Read for Data<R, Cursor<Vec<u8>>>
where
    R: io::Read + Unpin,
{
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        let mut reader: Pin<Box<dyn io::Read + Unpin>> = match self.get_mut() {
            Data::Owned(ref mut c) => Box::pin(c),
            Data::IO(ref mut r) => Box::pin(r),
            Data::None => Box::pin(Cursor::new(b"")),
        };
        reader.as_mut().poll_read(cx, buf)
    }
}
