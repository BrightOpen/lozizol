use crate::model::VuintError;
use std::{fmt, io};

/// An error type for reading vuint encoded integers.
#[derive(Debug)]
pub enum DecodeError {
    /// IO Error while reading.
    IoError(io::Error),
    /// Error converting bytes to number
    ConversionError(VuintError),
}

impl fmt::Display for DecodeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            DecodeError::IoError(ref e) => e.fmt(f),
            DecodeError::ConversionError(ref e) => e.fmt(f),
        }
    }
}

impl std::error::Error for DecodeError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match *self {
            DecodeError::IoError(ref e) => Some(e),
            DecodeError::ConversionError(ref e) => Some(e),
        }
    }
}

impl From<io::Error> for DecodeError {
    fn from(e: io::Error) -> Self {
        DecodeError::IoError(e)
    }
}
impl From<VuintError> for DecodeError {
    fn from(e: VuintError) -> Self {
        DecodeError::ConversionError(e)
    }
}
