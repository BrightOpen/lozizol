use crate::{
    model::{Data, Entry, Sequence},
    task::decode::ReadError,
};
use async_std::{
    channel::{bounded, Receiver, Sender},
    io::{self, Cursor},
    stream::StreamExt,
};
use std::{
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};

pub fn sequence<'s, 'r, 'f, R>(
    sequence: &'s mut Sequence,
    reader: R,
) -> SequenceStream<'s, 'r, 'f, R>
where
    R: io::Read + Unpin + 'r,
{
    SequenceStream {
        state: State::Ready { sequence, reader },
    }
}

pub struct SequenceStream<'s, 'r, 'f, R> {
    state: State<'s, 'r, 'f, R>,
}

type MyEntry<'r> = Entry<Box<dyn io::Read + Unpin + 'r>, Cursor<Vec<u8>>>;

enum State<'s, 'r, 'f, R> {
    Ready {
        sequence: &'s mut Sequence,
        reader: R,
    },
    Reading {
        receiver: Receiver<(&'s mut Sequence, R)>,
        read: Pin<Box<dyn Future<Output = Option<Result<MyEntry<'r>, ReadError>>> + 'f>>,
    },
    Waiting {
        receive: Pin<Box<dyn Future<Output = Option<(&'s mut Sequence, R)>> + 'f>>,
    },
    Invalid,
}

impl<'s, 'r, 'f, R> async_std::stream::Stream for SequenceStream<'s, 'r, 'f, R>
where
    R: io::Read + Unpin + 'r,
    's: 'f,
    'r: 'f,
    's: 'r,
{
    type Item = Result<MyEntry<'r>, ReadError>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        loop {
            break match std::mem::replace(&mut self.state, State::Invalid) {
                State::Ready {
                    sequence,
                    mut reader,
                } => {
                    let (sender, receiver) = bounded(1);
                    self.state = State::Reading {
                        receiver,
                        read: Box::pin(async move {
                            let mut entry = crate::task::decode::entry(sequence, &mut reader)
                                .await?
                                .expect("entry");

                            let position = *entry.position();
                            let length = *entry.len();
                            let type_uri = entry.type_uri().to_owned();
                            let data = entry.take_data();
                            drop(entry);

                            match data {
                                Data::IO(_) => {
                                    let reader: Box<dyn io::Read + Unpin> = Box::new(ReadAndSend {
                                        state: RState::Reading {
                                            sequence,
                                            reader,
                                            sender,
                                            remaining: length,
                                            length,
                                        },
                                    });
                                    Some(Ok(Entry::new(position, length, type_uri, Data::IO(reader))))
                                }
                                Data::Owned(cursor) => {
                                    sender.send((sequence, reader)).await;
                                    Some(Ok(Entry::new(position, length, type_uri, Data::Owned(cursor))))
                                }
                                Data::None => {
                                    sender.send((sequence, reader)).await;
                                    Some(Ok(Entry::new(position, length, type_uri, Data::None)))
                                }
                            }
                        }),
                    };
                    continue;
                }
                State::Reading {
                    mut receiver,
                    mut read,
                } => {
                    let poll = read.as_mut().poll(cx);
                    match poll {
                        Poll::Pending => {
                            self.state = State::Reading { receiver, read };
                            Poll::Pending
                        }
                        Poll::Ready(result) => {
                            self.state = State::Waiting {
                                receive: Box::pin(async move { receiver.next().await }),
                            };
                            Poll::Ready(result)
                        }
                    }
                }
                State::Waiting { mut receive } => match receive.as_mut().poll(cx) {
                    Poll::Pending => {
                        self.state = State::Waiting { receive };
                        Poll::Pending
                    }
                    Poll::Ready(Some((sequence, reader))) => {
                        self.state = State::Ready { sequence, reader };
                        continue;
                    }
                    Poll::Ready(None) => {
                        panic!("Reader must have been dropped without reading fully");
                    }
                },
                State::Invalid => {
                    unreachable!("invalid state must be transient")
                }
            };
        }
    }
}
struct ReadAndSend<'s, 'f, R> {
    state: RState<'s, 'f, R>,
}
enum RState<'s, 'f, R> {
    Reading {
        sequence: &'s mut Sequence,
        sender: Sender<(&'s mut Sequence, R)>,
        reader: R,
        remaining: usize,
        length: usize,
    },
    Sending(Pin<Box<dyn Future<Output = usize> + 'f>>),
    Invalid,
}
impl<'s, 'f, R> Drop for ReadAndSend<'s, 'f, R> {
    fn drop(&mut self) {
        match self.state {
            RState::Reading {
                remaining, length, ..
            } => error!(
                "reader dropped with {} out of {} remaining",
                remaining, length
            ),
            RState::Sending(_) => error!("reader dropped before sending back"),
            RState::Invalid => {}
        }
    }
}
impl<'s, 'r, 'f, R> io::Read for ReadAndSend<'s, 'f, R>
where
    R: io::Read + Unpin + 'r,
    'r: 'f,
    's: 'f,
{
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        loop {
            break match std::mem::replace(&mut self.state, RState::Invalid) {
                RState::Reading {
                    sequence,
                    mut reader,
                    sender,
                    mut remaining,
                    length,
                } => {
                    let mut rdr = Pin::new(&mut reader);
                    if let Poll::Ready(result) = rdr.as_mut().poll_read(cx, buf) {
                        match result {
                            Ok(len) => {
                                remaining -= len;
                                if remaining == 0 {
                                    self.state = RState::Sending(Box::pin(async move {
                                        sender.send((sequence, reader)).await.expect("send failed");
                                        len
                                    }));
                                    continue;
                                } else {
                                    self.state = RState::Reading {
                                        sequence,
                                        reader,
                                        sender,
                                        remaining,
                                        length,
                                    };
                                    Poll::Ready(Ok(len))
                                }
                            }
                            Err(e) => Poll::Ready(Err(e)),
                        }
                    } else {
                        self.state = RState::Reading {
                            sequence,
                            reader,
                            sender,
                            remaining,
                            length,
                        };
                        Poll::Pending
                    }
                }
                RState::Sending(mut fut) => {
                    if let Poll::Ready(len) = fut.as_mut().poll(cx) {
                        Poll::Ready(Ok(len))
                    } else {
                        self.state = RState::Sending(fut);
                        Poll::Pending
                    }
                }
                RState::Invalid => {
                    unreachable!("Invalid state must be transient")
                }
            };
        }
    }
}
