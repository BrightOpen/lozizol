use crate::{
    model::{Data, Record, Vuint},
    task::decode::DecodeError,
};
use async_std::io::{self, Cursor, ReadExt};
use std::{
    pin::Pin,
    task::{Context, Poll},
};

/// Read the record length and type and return a reader for the data.
/// The `Record::len()` is without the type bytes.
pub async fn record<'a, 'r, R>(
    mut input: R,
) -> Option<Result<Record<R, Cursor<Vec<u8>>>, DecodeError>>
where
    R: io::Read + Unpin + 'r,
    'r: 'a,
{
    let mut type_id = 0;
    let mut length = match super::vuint::number(&mut input).await {
        Err(DecodeError::IoError(e)) if e.kind() == io::ErrorKind::UnexpectedEof => return None,
        Err(other) => return Some(Err(other)),
        Ok(length) => length,
    };
    if length != 0 {
        type_id = match super::vuint::number(&mut input).await {
            Err(r) => return Some(Err(r)),
            Ok(n) => n,
        };
        length -= Vuint::bytes_needed(&type_id);
    }
    Some(Ok(Record::new(length, type_id, Data::IO(input))))
}

impl<R> Record<R, Cursor<Vec<u8>>>
where
    R: io::Read + Unpin,
{
    pub async fn skip_read(&mut self) -> io::Result<()> {
        match self.data {
            Data::None | Data::Owned(_) => self.remaining = 0,
            Data::IO(_) => {
                let mut buf = vec![];
                self.read_to_end(&mut buf).await?;
            }
        }
        Ok(())
    }
}

impl<R> io::Read for Record<R, Cursor<Vec<u8>>>
where
    R: io::Read + Unpin,
{
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        if self.remaining == 0 {
            return Poll::Ready(Ok(0));
        }
        let len = usize::min(buf.len(), self.remaining);
        let buf = &mut buf[..len];
        if let Poll::Ready(read) = Pin::new(&mut self.data).poll_read(cx, buf)? {
            self.remaining -= read;
            Poll::Ready(Ok(read))
        } else {
            Poll::Pending
        }
    }
}
