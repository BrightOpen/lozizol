use crate::model::{Data, Entry, Sequence};
use crate::task::decode::{DecodeError, HeaderReadError};
use async_std::io::{self, Cursor, ReadExt};
use std::{
    error::Error,
    pin::Pin,
    task::{Context, Poll},
};

pub async fn entry<'a, 'r, R>(
    sequence: &mut Sequence,
    reader: R,
) -> Option<Result<Entry<R, Cursor<Vec<u8>>>, ReadError>>
where
    R: io::Read + Unpin + 'r,
    'r: 'a,
{
    let position = sequence.read();

    match crate::task::decode::record(reader).await? {
        Err(e) => Some(Err(ReadError::from(e))),
        Ok(mut record) => {
            let rlen = record.len_including_header();
            sequence.advance_read(rlen);
            let length = *record.len();
            let type_uri = sequence
                .assignments()
                .get_uri(record.type_id())
                .expect(format!("type assignment for type id {}", record.type_id()).as_str())
                .to_owned();
            debug!("decoded {}@{} {:?}", rlen, position, type_uri);
            match type_uri.as_str() {
                "urn:lozizol:header" => {
                    let header =
                        match crate::task::decode::header_payload(&mut record, length).await {
                            Err(e) => return Some(Err(e.into())),
                            Ok(h) => h,
                        };
                    sequence.reset_from_header(&header);

                    Some(Ok(Entry::owned(
                        position,
                        length,
                        type_uri,
                        Cursor::new(header.into_vec()),
                    )))
                }
                "urn:lozizol:type" => {
                    let mut data = vec![0; length];
                    match record.read_exact(data.as_mut_slice()).await {
                        Err(e) => return Some(Err(e.into())),
                        Ok(_) => {}
                    };
                    let mut data_slice = data.as_slice();
                    let assign_id: usize = match crate::task::decode::number(&mut data_slice).await
                    {
                        Err(e) => return Some(Err(e.into())),
                        Ok(n) => n,
                    };
                    if data_slice.is_empty() {
                        sequence.assignments_mut().unset(&assign_id);
                    } else {
                        let uri: String = String::from_utf8_lossy(data_slice).into();
                        sequence.assignments_mut().set(assign_id, uri.clone());
                    }
                    Some(Ok(Entry::owned(
                        position,
                        length,
                        type_uri,
                        Cursor::new(data),
                    )))
                }
                _ => Some(Ok(Entry::from_record(record, position, type_uri))),
            }
        }
    }
}

impl<R> Entry<R, Cursor<Vec<u8>>>
where
    R: io::Read + Unpin,
{
    pub async fn skip_read(&mut self) -> io::Result<()> {
        match self.data {
            Data::None | Data::Owned(_) => self.remaining = 0,
            Data::IO(_) => {
                let mut buf = vec![];
                self.read_to_end(&mut buf).await?;
            }
        }
        Ok(())
    }
}

impl<R> io::Read for Entry<R, Cursor<Vec<u8>>>
where
    R: io::Read + Unpin,
{
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        if self.remaining == 0 {
            return Poll::Ready(Ok(0));
        }
        let len = usize::min(buf.len(), self.remaining);
        let buf = &mut buf[..len];

        if let Poll::Ready(read) = Pin::new(&mut self.data).poll_read(cx, buf)? {
            self.remaining -= read;
            Poll::Ready(Ok(read))
        } else {
            Poll::Pending
        }
    }
}

#[derive(Debug)]
pub enum ReadError {
    Header(HeaderReadError),
    Decode(DecodeError),
}

impl Error for ReadError {}
impl std::fmt::Display for ReadError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Sequence read error: ")?;
        match self {
            ReadError::Decode(e) => write!(f, "decoding error: {}", e),
            ReadError::Header(e) => write!(f, "{}", e),
        }
    }
}

impl From<HeaderReadError> for ReadError {
    fn from(e: HeaderReadError) -> Self {
        ReadError::Header(e)
    }
}
impl From<DecodeError> for ReadError {
    fn from(e: DecodeError) -> Self {
        ReadError::Decode(e)
    }
}

impl From<std::io::Error> for ReadError {
    fn from(e: std::io::Error) -> Self {
        ReadError::Decode(e.into())
    }
}
