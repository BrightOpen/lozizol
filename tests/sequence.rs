extern crate lozizol;

use lozizol::model::Vuint;

#[test]
fn powers() {
    for i in 0..120 {
        let int = u128::pow(2, i) - 1;
        println!("number {}", int);
        let vuint = Vuint::from(&int);
        let result: u128 = vuint.to_uint().expect("must round trip");
        assert_eq!(
            result, int,
            "\n{:b},\nint: {:b},\nres: {:b}",
            vuint, int, result
        )
    }
}

#[test]
fn ones() {
    for i in 0..16 {
        let mut int = [0u8; 16];

        int[15 - i] = 255;

        let int = u128::from_be_bytes(int);
        println!("number {}", int);
        let vuint = Vuint::from(&int);
        let result: u128 = vuint.to_uint().expect("must round trip");
        assert_eq!(
            result, int,
            "\n{:b},\nint: {:b},\nres: {:b}",
            vuint, int, result
        )
    }
}
