extern crate lozizol;

use cucumber_rust::{async_trait, given, then, when, World, WorldInit};
use lozizol::model::Vuint;
use std::convert::Infallible;

#[derive(Default, WorldInit)]
pub struct MyWorld {
    // You can use this struct for mutable context in scenarios.
    decimal: u128,
    vuint: Vuint<'static>,
    binary: Vec<u8>,
}

impl MyWorld {}

#[given(regex = "^an unsigned integer '([0-9]+)'$")]
async fn set_uint(world: &mut MyWorld, n: u128) {
    world.decimal = n
}

#[when("I serialize it as vuint")]
async fn serialize_vuint(world: &mut MyWorld) {
    world.vuint = Vuint::from(&world.decimal);
    world.binary = world.vuint.as_ref().to_owned();
}

#[when("I deserialize it as vuint")]
async fn deserialize_vuint(world: &mut MyWorld) {
    world.vuint = Vuint::try_from_bytes(world.binary.clone()).expect("must be valid");
    world.decimal = world.vuint.to_uint().expect("must convert");
}

#[then(regex = "^the decimal representation should be '(.*)'$")]
async fn assert_decimal(world: &mut MyWorld, n: u128) {
    assert_eq!(
        format!("{:?}", world.decimal.to_be_bytes()),
        format!("{:?}", n.to_be_bytes())
    );
}

#[then(regex = "^the binary representation should be '(.*)'$")]
async fn assert_binary(world: &mut MyWorld, b: String) {
    assert_eq!(format!("{:?}", world.vuint.as_ref()), format!("[{}]", b))
}

#[async_trait(?Send)]
impl World for MyWorld {
    type Error = Infallible;

    async fn new() -> Result<Self, Infallible> {
        Ok(Self::default())
    }
}

fn main() {
    let runner = MyWorld::init(&["./features"]);
    futures::executor::block_on(runner.run_and_exit());
}
