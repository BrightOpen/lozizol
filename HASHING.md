# Hashing

This is for now just an example.

Use case: integrity of the records must be verified.

## Entries

### Container hash

Use case: the hash is known ahead of the write or can be back filled.

CRC-32C is a very fast and compact integrity check. It is not cryptographically
sound nor collision resistant and should not be used for anything but integrity 
check. Since there are many specifications of CRC-32, here we specifically 
select the CRC-32C (Castagnoli) flavor (polyn. `0x1EDC6F41`).

* URI: `urn:lozizol:container-hash-crc-32c`
* Properties:
  * hash (4 bytes) - the CRC-32C checksum of the hashed records
  * list of hashed entries

#### Record

  * size (`vuint`) - length of type, 4 byte hash and all contained records
  * type (`vuint`) - type id assigned to this entry type
  * hash (4 bytes) 
  * records of hashed entries

### Normal hash

Use case: the hash must/should be written after the hashed records.

CRC-32C is a very fast and compact integrity check. It is not cryptographically
sound nor collision resistant and should not be used for anything but integrity 
check. Since there are many specifications of CRC-32, here we specifically 
select the CRC-32C (Castagnoli) flavor (polyn. `0x1EDC6F41`).

* URI: `urn:lozizol:normal-hash-crc-32c`
* Properties:
  * hash (4 bytes) - the CRC-32C checksum of the hashed records
  * list of hashed entries

#### Record

  * size (`vuint`) - length of type, 4 byte hash and all contained record positions
  * type (`vuint`) - type id assigned to this entry type
  * hash (4 bytes) 
  * position* (`vuint*`) - positions of all hashed record in the sequence
